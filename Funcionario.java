public class Funcionario

{
	public String nome;
	public double salario;
	public String rg;
	public int horasTrabalhadas;
	public Data dataDeEntrada;
	
	public void recebeAumento(double valorAumento)
	
	{
		this.salario= this.salario + valorAumento;
	}

	public double calculaGanhoAnual() 
	
	{
		return this.salario*12;
	}

	
	public double valorHora() 

	{
		return this.salario/horasTrabalhadas;
	}

	public void mostra() 

	{
		System.out.println("\n");
		System.out.println("Nome: " + this.nome);
		System.out.println("salário: " + this.salario);
		System.out.println("RG: " + this.rg);
		System.out.println("Horas Trabalhadas: " + this.horasTrabalhadas);
		System.out.println("Ganho Anual: " + this.calculaGanhoAnual());
		System.out.println("Data de Entrada:  " + this.dataDeEntrada.valorFormatado());
		System.out.println("\n");

	}
	
}	
